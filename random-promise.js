var express = require('express'),
    webRequest = require('./web-request'),
    querystring = require("querystring"),
    _ = require('underscore'),
    Q = require('q'),
    router = express.Router();


var encodeJoke = function(response) {
    var randomJoke = JSON.parse(response).value;
    return querystring.escape(randomJoke);
};

// Takes random chucknorris jokes and translates it pirate talk
// using: https://api.chucknorris.io
//        http://isithackday.com/arrpi.php
router.get('/', function(req, res) {
    webRequest('https://api.chucknorris.io/jokes/random')
        .then(function(response) {
            return webRequest("http://isithackday.com/arrpi.php?text=" + encodeJoke(response));
        })
        .then(function(response) {
            res.send(response);
        })
        .catch(function(error) {
            res.status(500).send('Server failure:' + error);
        })
        .done();
});

// Stump for request to handle categories
// /categories?cat=dev,food
// categories: ["explicit","dev","movie","food","celebrity","science","political","sport",
// "religion","animal","music","history","travel","career","money","fashion"]
router.get('/categories', function(req, res) {
    var catString = req.query.cat;
    var categories = catString.split(',');
    var promises = [];
    categories.forEach(function(category) {
        promises.push(webRequest("https://api.chucknorris.io/jokes/random?category=" + category));
    });
    Q.all(promises)
        .then(function(responses) {
            return Q.all(_.map(responses, function(response) {
                return webRequest("http://isithackday.com/arrpi.php?text=" + encodeJoke(response));
            }));
        })
        .then(function(responses) {
            return res.send(responses.join('<br/>'));
        })
        .catch(function(error) {
            res.status(500).send('Server failure:' + error);
        })
        .done();
});

module.exports = router;
