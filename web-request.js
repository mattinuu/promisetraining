var request = require('request'),
    Q = require('q');

module.exports = function(url) {
    var deferred = Q.defer();
    request(url, function(error, response, body) {
        if (!error) {
            deferred.resolve(body);
        } else {
            deferred.reject(error);
        }
    });
    return deferred.promise;
};
