var express = require('express'),
    app = express(),
    router = express.Router();

var random = require('./random-promise');

app.use(router);

router.use('/', random);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
