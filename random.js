var express = require('express'),
    request = require('request'),
    querystring = require("querystring"),
    router = express.Router();


// Takes random chucknorris jokes and translates it pirate talk
// using: https://api.chucknorris.io
//        http://isithackday.com/arrpi.php
router.get('/', function(req, res) {
    request('https://api.chucknorris.io/jokes/random', function(error, response, body) {
        if(!error) {
            var randomJoke = JSON.parse(body).value;
            var uriEncodedJoke = querystring.escape(randomJoke);
            request("http://isithackday.com/arrpi.php?text=" + uriEncodedJoke, function(error2, response2, body2) {
                if(!error2) {
                    res.send(body2);
                } else {
                    res.status(500).send('Server failure');
                }
            });
        } else {
            res.status(500).send('Server failure');
        }
    });
});

// Stump for request to handle categories
router.get('/categories', function(req, res) {
    var catString = req.param('cat');
    var categories = catString.split(',');
    // ...
    res.send('Not implemented');
});

module.exports = router;
